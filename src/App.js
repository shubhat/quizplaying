import React from "react";
import { MainApp } from "./components/view/Index";

import { getQuizDetails } from "./components/util";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.fetchQuizData = this.fetchQuizData.bind(this);
    this.state = {
      rquestedQuizId: props.requestedQuizId,
      completedQuizId: props.completedQuizId
    };

    this.fetchQuizData();
  }

  async fetchQuizData() {
    let requestedQuizId = this.props.requestedQuizId;
    let completedQuizId = this.props.completedQuizId;

    let requestedQuizDetails;
    let completedQuizDetails;

    const reflect = p =>
      p.then(
        v => ({ v, status: "fulfilled" }),
        e => ({ e, status: "rejected" })
      );

    let details = [
      getQuizDetails(requestedQuizId),
      getQuizDetails(completedQuizId)
    ];

    Promise.all(details.map(reflect)).then(results => {
      requestedQuizDetails = results[0].v;
      completedQuizDetails = results[1].v;

      if (requestedQuizDetails === undefined) {
        requestedQuizId = undefined;
      }

      if (completedQuizDetails === undefined) {
        completedQuizId = undefined;
      }

      this.setState({
        rquestedQuizId: requestedQuizId,
        completedQuizId: completedQuizId,
        requestedQuizDetails: requestedQuizDetails,
        completedQuizDetails: completedQuizDetails
      });
    });
  }

  render() {
    return (
      <MainApp
        rquestedQuizId={this.state.rquestedQuizId}
        requestedQuizDetails={this.state.requestedQuizDetails}
        completedQuizId={this.state.completedQuizId}
        completedQuizDetails={this.state.completedQuizDetails}
      />
    );
  }
}

export default App;
