import React from "react";
import ReactDOM from "react-dom";
import "./css/index.css";
import "./index.css";
import App from "./App";
import "justgage";

import { getCompletedQuizId, getUrlQuizId } from "./components/util";

export default window.JustGage;
ReactDOM.render(
  <div className="text-center">
    <App
      completedQuizId={getCompletedQuizId()}
      requestedQuizId={getUrlQuizId()}
    />
  </div>,
  document.getElementById("root")
);
