import { questions } from "../data/questions";

export class Questions {
  constructor(requetorName, filter) {
    let allQuestions = [...questions];

    if (filter) {
      const answeredQuestions = filter.map(o => o.qId);
      allQuestions = allQuestions.filter(e => answeredQuestions.includes(e.id));
    }

    if (requetorName) {
      allQuestions = allQuestions.map(q => {
        let stringified = JSON.stringify(q);
        stringified = stringified.replace(
          /{user}/g,
          requetorName.charAt(0).toUpperCase() + requetorName.slice(1)
        );
        return JSON.parse(stringified);
      });
    }
    const shuffled = shuffle(allQuestions);
    let totalFeatured = [...shuffled];
    if (!filter) {
      totalFeatured = getFeatured(shuffled);
    }

    this.featured = totalFeatured.slice(0, 20);
    this.nonFeatured = [
      ...getNonFeatured(shuffled),
      ...totalFeatured.slice(20)
    ];
    this.activeFeatured = -1;
    this.activeNonFeatured = -1;
  }

  getLength() {
    return this.featured.length;
  }

  getNextFeatured() {
    this.activeFeatured += 1;
    if (this.activeFeatured >= this.featured.length) {
      this.activeFeatured = 0;
    }

    return this.featured[this.activeFeatured];
  }

  getNextImages() {
    let nextFeatured = this.activeFeatured + 1;
    if (nextFeatured >= this.featured.length) {
      nextFeatured = 0;
    }

    const images = this.featured[nextFeatured].images.map(i => i.url);

    let nextNonFeatured = this.activeNonFeatured + 1;
    if (nextFeatured >= this.nonFeatured.length) {
      nextNonFeatured = 0;
    }

    return [
      ...images,
      ...this.featured[nextNonFeatured].images.map(i => i.url)
    ];
  }

  getNextNonFeatured() {
    this.activeNonFeatured += 1;
    if (this.activeNonFeatured > this.nonFeatured.length) {
      this.activeNonFeatured = 0;
    }

    return this.nonFeatured[this.activeNonFeatured];
  }
}

const shuffle = data => {
  for (let i = data.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [data[i], data[j]] = [data[j], data[i]];
  }
  return data;
};

const getFeatured = data => data.filter(d => d["featured"] === 1);
const getNonFeatured = data => data.filter(d => d["featured"] !== 1);
