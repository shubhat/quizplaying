import Cookies from "universal-cookie";

var URL = require("url-parse");
const axios = require("axios");

const baseUrl = "https://igzeo4fh0b.execute-api.ap-south-1.amazonaws.com/api/";

export const getQuizDetails = async quizId => {
  if (!quizId) {
    return;
  }

  const result = await axios.get(`${baseUrl}get/${quizId}`);
  return result ? result.data : undefined;
};

export const compareAnswers = (originalAnswers, newAnswers) => {
  const o = originalAnswers.reduce(
    (accumulate, originalAnswer) => ({
      ...accumulate,
      [originalAnswer.qId]: originalAnswer.aId
    }),
    {}
  );

  const n = newAnswers.reduce(
    (accumulate, newAnswer) => ({
      ...accumulate,
      [newAnswer.qId]: newAnswer.aId
    }),
    {}
  );
  return Object.keys(n).filter(k => n[k] === o[k]).length;
};

export const saveQuizDetails = async questions => {
  const name = getName();
  const result = await axios.post(`${baseUrl}saveUserDetails`, {
    questions: questions,
    name: name
  });
  if (result) {
    const data = result.data;
    saveCompletedQuizId(data.userId);
    return data;
  }
};

export const saveFriendDetails = async (quizId, score) => {
  const result = await axios.post(`${baseUrl}saveFriendsScores/${quizId}`, {
    scores: {
      score: score,
      name: getName()
    }
  });

  if (result) {
    return result.data;
  }
};

export const getUrlQuizId = () => {
  const url = new URL(window.location.href);
  const quizId = (url.pathname || "").replace("/quiz/", "").replace("/", "");
  if (quizId) {
    return quizId;
  }
};

export const getName = () => {
  const cookies = new Cookies();
  return cookies.get("userName");
};

export const getCompletedQuizId = () => {
  const cookies = new Cookies();
  return cookies.get("completed_quiz_id");
};

export const logout = () => {
  const cookies = new Cookies();
  cookies.remove("completed_quiz_id", { path: "/" });
  cookies.remove("userName", { path: "/" });
  window.location.href = "/";
};

export const saveCompletedQuizId = quizId => {
  const cookies = new Cookies();
  return cookies.set("completed_quiz_id", quizId, {
    path: "/",
    expires: new Date(new Date().getTime() + 30000000000)
  });
};

export const getQuizScore = quizId => {
  const cookies = new Cookies();
  return cookies.get(`completed_${quizId}`);
};

export const clearQuizScore = quizId => {
  const cookies = new Cookies();
  cookies.remove(`completed_${quizId}`, { path: "/" });
  window.location.reload();
};

export const saveCompletedQuizScore = (quizId, score) => {
  const cookies = new Cookies();
  cookies.set(`completed_${quizId}`, score, {
    path: "/",
    expires: new Date(new Date().getTime() + 30000000000)
  });
};

export const isQuizCompleted = quizId => getQuizScore(quizId) !== undefined;
