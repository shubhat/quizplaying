import React from "react";
import { logout } from "../components/util";

export const Nav = ({ color, showLogout }) => (
  <div
    className="text-center justify-center text-white flex"
    style={{ background: color }}
  >
    <div className="flex flex-1 justify-center items-center">
      <div className="flex-1" />
      <div className="flex justify-center items-center ">
        <FriendshipIcon />
        <a
          href="/"
          className="flex-1 p-1 text-xl tracking-wide leading-loose hover:no-underline hover:text-white hover:font-semibold"
        >
          2019 Friendship Dare{" "}
        </a>
        <FriendshipIcon />
      </div>
      <div className="flex-1 text-right">
        {showLogout && (
          <button
            className="mr-3 text-lg"
            onClick={() => {
              logout();
            }}
          >
            {" "}
            Logout{" "}
          </button>
        )}
      </div>
    </div>
  </div>
);

const FriendshipIcon = () => (
  <span className="px-3 pt-1">
    <svg
      style={{ height: "26px", width: "26px" }}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
      className="feather feather-heart"
    >
      <path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z" />
    </svg>
  </span>
);
