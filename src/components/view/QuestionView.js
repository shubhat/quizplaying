import React from "react";
import { UIColorPallet } from "../UIColorPalette";
import { Pagination } from "../Pagination";
import { SmallButton, Heading } from "../index";
import { Questions } from "../util/Questions";
import { AnswerImageGroup } from "../AnswerImageGroup";
import { SavingResultView } from "../view/SavingResultView";

// setColor, color, originalQuizId, originalAnswers, requestorName
export class QuestionView extends React.Component {
  constructor(props) {
    super(props);

    this.skipQuestion = this.skipQuestion.bind(this);
    this.imageClicked = this.imageClicked.bind(this);

    const { requestorName, originalAnswers } = this.props;

    let q = new Questions(requestorName, originalAnswers);

    this.state = {
      active: 1,
      questions: q,
      activeQuestion: q.getNextFeatured(),
      answers: [],
      size: Math.min(q.getLength(), 20)
    };
  }

  componentDidUpdate() {
    this.state.questions.getNextImages().forEach(picture => {
      const img = new Image();
      img.src = `/images/${picture}`;
    });
  }

  skipQuestion() {
    setTimeout(() => {
      this.setState({
        activeQuestion: this.state.questions.getNextNonFeatured()
      });
    }, 150);
  }

  imageClicked(imageId) {
    if (this.state.processing) {
      return;
    }
    let c = "";
    if (this.props.originalAnswers) {
      const activeQ = this.props.originalAnswers.filter(
        o => o.qId === this.state.activeQuestion.id
      );
      if (activeQ && activeQ[0].aId === imageId) {
        c = "right";
      } else {
        c = "wrong";
      }
    } else {
      c = "selected";
    }

    if (c) {
      document.querySelector(`.img-${imageId}`).classList.add(c);
    }
    setTimeout(() => {
      this.setState({ processing: false });
      if (c) {
        document.querySelector(`.img-${imageId}`).classList.remove(c);
      }
      this.setState(({ active, answers, activeQuestion }) => ({
        activeQuestion: this.state.questions.getNextFeatured(),
        active: active + 1,
        answers: [
          ...answers,
          {
            qId: activeQuestion.id,
            aId: imageId
          }
        ]
      }));
    }, 750);
    this.setState({ processing: true });
  }

  render() {
    const { originalQuizId, originalAnswers } = this.props;
    if (this.state.active - 1 >= this.state.size) {
      return (
        <SavingResultView
          originalQuizId={originalQuizId}
          originalAnswers={originalAnswers}
          newAnswers={this.state.answers}
          {...this.props}
        />
      );
    }

    const { color, setColor } = this.props;
    return (
      <div className="max-w-3xl px-3 p-2 bg-gray-100 border-2 border-gray-600 rounded-xlg mt-3">
        <div className="flex justify-center mt-2">
          <UIColorPallet setColor={setColor} />
        </div>
        <div className="text-center flex flex-wrap justify-center">
          <Pagination size={this.state.size} active={this.state.active} />
        </div>
        {!originalQuizId && (
          <div className="flex justify-center">
            <div className="mt-3 w-1/2 flex justify-center">
              <SmallButton
                text="Skip this question"
                color={color}
                onClick={this.skipQuestion}
              />
            </div>
          </div>
        )}
        <div className="text-center mt-3">
          <Heading
            title={
              this.props.requestorName
                ? this.state.activeQuestion.answer
                : this.state.activeQuestion.question
            }
          />
        </div>
        <div className="">
          <AnswerImageGroup
            answers={this.state.activeQuestion.images}
            imageClicked={this.imageClicked}
          />
        </div>
      </div>
    );
  }
}
