import React from "react";
import { Heading, SubHeading, Button } from "../index";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { ResultTable } from "../ResultTable";
import { clearQuizScore, getQuizScore, getName } from "../util/index";

// quizId, color, score, rows, name
export class EndView extends React.Component {
  render() {
    const { quizId, color, requestedQuizId, rows, name } = this.props;

    const score = getQuizScore(requestedQuizId);

    return (
      <div className="text-center max-w-3xl px-3 p-2 shadow bg-gray-100 border-2 border-gray-600 rounded-xlg mt-3">
        <QuizHeader quizId={quizId} color={color} />
        <ScoreHeader
          score={score}
          color={color}
          requestedQuizId={requestedQuizId}
        />
        <Footer rows={rows} name={name} />
      </div>
    );
  }
}

class ScoreHeader extends React.Component {
  componentDidMount() {
    const { score } = this.props;
    if (!score) {
      return false;
    }
    new window.JustGage({
      id: "gauge",
      value: score,
      min: 0,
      max: 20,
      pointer: true,
      pointerOptions: {
        toplength: -15,
        bottomlength: 10,
        bottomwidth: 12,
        color: "#8e8e93",
        stroke: "#ffffff",
        stroke_width: 3,
        stroke_linecap: "round"
      },
      gaugeWidthScale: 0.8,
      counter: true
    });
  }
  render() {
    const { score, color } = this.props;
    if (!score) {
      return false;
    }
    return (
      <>
        <div className="-mb-5">
          <div className="text-2xl font-bold">Your Score : {score}/20</div>
        </div>
        <div className="w-full">
          <div id="gauge" style={{ height: "200px" }} />
        </div>
        <div className="w-full p-2 flex">
          <div className="flex-1 mr-3">
            <Button
              text="Create your own quiz"
              onClick={() => (window.location.href = "/")}
              color={color}
            />
          </div>
          {this.props.score && (
            <div className="flex-1">
              <Button
                text="Try Again"
                onClick={() => {
                  clearQuizScore(this.props.requestedQuizId);
                }}
                color={color}
              />
            </div>
          )}
        </div>
      </>
    );
  }
}

const QuizHeader = ({ quizId, color }) => {
  if (!quizId) {
    return false;
  }
  const url = `${window.location.protocol}//${
    window.location.hostname
  }/quiz/${quizId}`;

  return (
    <>
      <Heading title="Your Challenge is Ready" />
      <SubHeading title="Share this link with your friends" />
      <div className="mt-3">
        <CopyToClipboard text={url}>
          <div className="border bg-white py-2 text-md md:text-lg rounded-xlg">
            {url}
          </div>
        </CopyToClipboard>
      </div>
      <div className="mt-3 mx-auto w-64" data-tooltip="Copied">
        <CopyToClipboard
          text={url}
          onCopy={() => {
            [...document.querySelectorAll("[data-tooltip]")].forEach(a =>
              a.classList.add("hover")
            );
            setTimeout(() => {
              [...document.querySelectorAll("[data-tooltip]")].forEach(a =>
                a.classList.remove("hover")
              );
            }, 1000);
          }}
        >
          <Button text="Copy this link" color={color} />
        </CopyToClipboard>
      </div>
      <div className="my-1 flex">
        <SharableIcon color="#1cb06d" icon={<WhatsAppIcon/>} href={`https://api.whatsapp.com/send?text=🤜 ${getName()} has sent you real *2019 Friendship Challenge* 👸🤴️%0A*Accept this Challenge NOW* %0A‼️👇👇👇👇👇👇‼️ %0A  ${url}`}/>
        <SharableIcon color="linear-gradient(45deg,#f09433 0,#e6683c 25%,#dc2743 50%,#cc2366 75%,#bc1888 100%)" icon="Add to Instagram Bio" href="https://www.instagram.com/accounts/edit/"/>
      </div>
      <SharableIcons url={url} name={getName()} />
    </>
  );
};

const Footer = ({ rows, name }) => {
  return (
    <div className="mt-3">
      <ResultTable rows={rows} name={name} />
    </div>
  );
};

class SharableIcons extends React.Component {
  componentDidMount() {
    setTimeout(() => {
      var addthisScript = document.createElement("script");
      addthisScript.setAttribute(
        "src",
        "https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55bb64a351c52412"
      );
      if (document.body) document.body.appendChild(addthisScript);
      const interval = setInterval(() => {
        var addthis_config = window.addthis_config||{};
        if (Object.keys(addthis_config).length) {
          clearInterval(interval);
        }
        addthis_config.data_track_addressbar = false;
        addthis_config.data_track_clickback = false;
      }, 500);
    });
  }

  render() {
    const { url, name } = this.props;

    const title = `🤜 ${name} has sent you real 2019 Friendship Challenge 👸🤴️. Accept this Challenge NOW ‼️👇👇👇👇👇👇‼️ `;
    return (
      <div
        className="addthis_inline_share_toolbox mt-3"
        data-url={url}
        data-title={title}
        data-description={title}
      />
    );
  }
}

const SharableIcon = ({ icon, color, href }) => (
    <a
      className=" flex-1 m-2 p-2 border rounded-lg text-sm md:text-xl font-semibold leading-relaxed text-white"
      style={{ background: color }}
      href={href}
      target="_blank"
      rel="noopener noreferrer"
    >
      {icon}
    </a>
);

const WhatsAppIcon = () => (
  <div className="flex items-center justify-center">
  <svg className="mr-2" 
  xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="18" height="18" viewBox="0 0 90 90"><g>
								<path id="WhatsApp" d="M90,43.841c0,24.213-19.779,43.841-44.182,43.841c-7.747,0-15.025-1.98-21.357-5.455L0,90l7.975-23.522   c-4.023-6.606-6.34-14.354-6.34-22.637C1.635,19.628,21.416,0,45.818,0C70.223,0,90,19.628,90,43.841z M45.818,6.982   c-20.484,0-37.146,16.535-37.146,36.859c0,8.065,2.629,15.534,7.076,21.61L11.107,79.14l14.275-4.537   c5.865,3.851,12.891,6.097,20.437,6.097c20.481,0,37.146-16.533,37.146-36.857S66.301,6.982,45.818,6.982z M68.129,53.938   c-0.273-0.447-0.994-0.717-2.076-1.254c-1.084-0.537-6.41-3.138-7.4-3.495c-0.993-0.358-1.717-0.538-2.438,0.537   c-0.721,1.076-2.797,3.495-3.43,4.212c-0.632,0.719-1.263,0.809-2.347,0.271c-1.082-0.537-4.571-1.673-8.708-5.333   c-3.219-2.848-5.393-6.364-6.025-7.441c-0.631-1.075-0.066-1.656,0.475-2.191c0.488-0.482,1.084-1.255,1.625-1.882   c0.543-0.628,0.723-1.075,1.082-1.793c0.363-0.717,0.182-1.344-0.09-1.883c-0.27-0.537-2.438-5.825-3.34-7.977   c-0.902-2.15-1.803-1.792-2.436-1.792c-0.631,0-1.354-0.09-2.076-0.09c-0.722,0-1.896,0.269-2.889,1.344   c-0.992,1.076-3.789,3.676-3.789,8.963c0,5.288,3.879,10.397,4.422,11.113c0.541,0.716,7.49,11.92,18.5,16.223   C58.2,65.771,58.2,64.336,60.186,64.156c1.984-0.179,6.406-2.599,7.312-5.107C68.398,56.537,68.398,54.386,68.129,53.938z" fill="#eeeeee"></path></g>
							</svg>
  Whatsapp Status
  </div>
)
