import React from "react";
import { SubHeading, Heading } from "../index";
import { UIColorPallet } from "../UIColorPalette";
import {
  saveQuizDetails,
  compareAnswers,
  saveFriendDetails,
  getQuizDetails,
  saveCompletedQuizScore
} from "../util";
import { EndView } from "../view/EndView";

// originalQuizId, newAnswers, originalAnswers, setColor, color
export class SavingResultView extends React.Component {
  constructor(props) {
    super(props);
    this.saveNewUserAnswers = this.saveNewUserAnswers.bind(this);
    this.saveFriendAnswers = this.saveFriendAnswers.bind(this);

    const { originalQuizId, newAnswers } = this.props;

    if (!originalQuizId) {
      this.saveNewUserAnswers(newAnswers);
    } else {
      const { originalAnswers } = this.props;
      this.saveFriendAnswers(originalQuizId, originalAnswers, newAnswers);
    }
    this.state = {
      title: ""
    };
  }

  async saveNewUserAnswers(q) {
    this.setState({ title: "Creating Quiz" });
    const response = await saveQuizDetails(q);
    if (response) {
      this.setState({
        quizId: response.userId,
        rows: [],
        name: "You",
        saved: true
      });
    }
  }

  async saveFriendAnswers(originalQuizId, originalAnswers, newAnswers) {
    this.setState({ title: "Calculating Scores" });
    const score = compareAnswers(originalAnswers, newAnswers);
    await saveFriendDetails(originalQuizId, score);
    const result = await getQuizDetails(originalQuizId);
    saveCompletedQuizScore(originalQuizId, score);
    this.setState({
      rows: result ? result.scores : undefined,
      name: result ? result.name : undefined,
      saved: true
    });
  }

  render() {
    if (this.state.saved) {
      return (
        <EndView
          {...this.props}
          quizId={this.state.quizId}
          requestedQuizId={this.props.originalQuizId}
          rows={this.state.rows}
          name={this.state.name}
        />
      );
    }

    return (
      <div className="max-w-3xl px-3 p-2 bg-gray-100 border-2 border-gray-600 rounded-xlg mt-3">
        <Heading title="2019 Friendship Dare" />
        <div className="flex justify-center mt-2">
          <UIColorPallet setColor={this.props.setColor} />
        </div>
        <SubHeading title="Please Wait!" />
        <SubHeading title={this.state.title} />
        <div className="mt-2 flex justify-center">
          <img src="https://i.imgur.com/fvoOJ5h.gif" alt="wait" />
        </div>
      </div>
    );
  }
}
