import React from "react";

import { Nav } from "../Nav";
import { StartView } from "./StartView";
import { EndView } from "../view/EndView";
import { Heading } from "../index";
import { isQuizCompleted } from "../util";
import { AddInTop, AddInBottom } from "../AddIn";

export class MainApp extends React.Component {
  constructor(props) {
    super(props);

    this.setColor = this.setColor.bind(this);
    this.state = {
      color: "#fc4d21"
    };
  }

  setColor(color) {
    this.setState({ color });
  }

  render() {
    let entry = undefined;
    if (this.props.rquestedQuizId) {
      if (this.props.rquestedQuizId === this.props.completedQuizId) {
        entry = (
          <EndView
            color={this.state.color}
            setColor={this.setColor}
            quizId={this.props.completedQuizId}
            name="You"
            rows={
              this.props.completedQuizDetails
                ? this.props.completedQuizDetails.scores
                : []
            }
          />
        );
      } else {
        if (!this.props.requestedQuizDetails) {
          entry = (
            <div className="max-w-3xl px-3 p-2 bg-gray-100 border-2 border-gray-600 rounded-xlg mt-3">
              <div className=" flex flex-column item-center justify-center">
                <Heading title="loading" />
                <div className="my-5 flex-5 h-8 w-8 mx-auto">
                  <img
                    className="bg-cover mx-auto"
                    src="/images/loadig.gif"
                    alt="loading"
                  />
                </div>
              </div>
            </div>
          );
        } else {
          if (isQuizCompleted(this.props.rquestedQuizId)) {
            entry = (
              <EndView
                color={this.state.color}
                setColor={this.setColor}
                requestedQuizId={this.props.rquestedQuizId}
                rows={
                  this.props.requestedQuizDetails
                    ? this.props.requestedQuizDetails.scores
                    : []
                }
                name={
                  this.props.requestedQuizDetails
                    ? this.props.requestedQuizDetails.name
                    : ""
                }
              />
            );
          } else {
            entry = (
              <StartView
                color={this.state.color}
                setColor={this.setColor}
                originalQuizId={this.props.rquestedQuizId}
                originalAnswers={
                  this.props.requestedQuizDetails
                    ? this.props.requestedQuizDetails.questions
                    : []
                }
                requestorName={
                  this.props.requestedQuizDetails
                    ? this.props.requestedQuizDetails.name
                    : ""
                }
                rows={
                  this.props.requestedQuizDetails
                    ? this.props.requestedQuizDetails.scores
                    : []
                }
              />
            );
          }
        }
      }
    } else {
      if (this.props.completedQuizId) {
        entry = (
          <EndView
            color={this.state.color}
            setColor={this.setColor}
            quizId={this.props.completedQuizId}
            name="You"
            rows={
              this.props.completedQuizDetails
                ? this.props.completedQuizDetails.scores
                : []
            }
          />
        );
      } else {
        entry = <StartView color={this.state.color} setColor={this.setColor} />;
      }
    }
    return (
      <>
        <Nav
          color={this.state.color}
          href="/"
          showLogout={this.props.completedQuizId}
        />
        <AddInTop />
        <div className="max-w-2xl mx-auto p-2">{entry}</div>
        <AddInBottom />
        <div className="max-w-2xl mx-auto p-2 font-semibold text-gray-700 antialiased leading-relaxed">
          Enter your Name, Create your Quiz and Share it with your friends on
          Facebook or Whatsapp. Once your friends attempt the quiz you will see
          the results on leaderboard.
        </div>
      </>
    );
  }
}
