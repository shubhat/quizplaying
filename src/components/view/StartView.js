import React from "react";
import { UIColorPallet } from "../UIColorPalette";
import { SubHeading, Heading, Button } from "../index";
import { ResultTable } from "../ResultTable";
import Cookies from "universal-cookie";
import { QuestionView } from "../view/QuestionView";

// setColor, color, originalQuizId, originalAnswers, requestorName, rows

export class StartView extends React.Component {
  constructor(props) {
    super(props);

    this.updateUserName = this.updateUserName.bind(this);
    this.setUserName = this.setUserName.bind(this);

    const cookies = new Cookies();
    this.state = {
      userName: cookies.get("userName") || "",
      view: "StartView"
    };
  }

  updateUserName(evt) {
    this.setState({ userName: evt.target.value });
  }

  setUserName(userName) {
    let isValid =
      userName.match(/[A-Za-z ]+/) !== null &&
      userName.match(/[A-Za-z ]+/)[0] === userName;

    const invalidCharacters = [
      "butt",
      "sex",
      "fuck",
      "shit",
      "tit",
      "pussy",
      "ass",
      "tits",
      "porn",
      "sexy",
      "xnxx",
      "xxx",
      "horny",
      "xvideo",
      "xvideos",
      "pornhub",
      "gangbang",
      "boobs",
      "dick",
      "cock",
      "milf",
      "cumshot",
      "anal"
    ];
    if (!isValid) {
      [...document.querySelectorAll("[data-tooltip]")].forEach(a =>
        a.classList.add("hover")
      );
      setTimeout(() => {
        [...document.querySelectorAll("[data-tooltip]")].forEach(a =>
          a.classList.remove("hover")
        );
      }, 1000);
      return;
    }

    let name = userName;
    invalidCharacters.map(
      i => (name = name.toLocaleLowerCase().replace(i, ""))
    );
    const cookies = new Cookies();
    cookies.set("userName", name || "", {
      path: "/",
      expires: new Date(new Date().getTime() + 30000000000)
    });

    this.setState({ view: "QuestionView" });
  }

  render() {
    if (this.state.view === "QuestionView") {
      return <QuestionView {...this.props} />;
    }
    return (
      <div className="max-w-2xl px-3 p-2 border-2 bg-gray-100 border-gray-600 rounded-xlg mt-4">
        <Header {...this.props} />

        <StartViewUserInput
          {...this.props}
          setUserName={this.setUserName}
          updateUserName={this.updateUserName}
          userName={this.state.userName}
        />

        <div className="text-center mt-4">
          <ResultTable {...this.props} name={this.props.requestorName} />
        </div>
      </div>
    );
  }
}

const Header = ({ setColor, requestorName }) => {
  if (!requestorName) {
    return (
      <>
        <div className="flex justify-center">
          <UIColorPallet setColor={setColor} />
        </div>
        <Heading title="2019 Friendship Dare" />
      </>
    );
  } else {
    return (
      <div className="text-center">
        <Heading title="2019 Friendship Dare" />
        <div className="mt-3">
          <SubHeading
            title={
              <>
                <b>{requestorName}</b> has invited you to Friendship Dare Test
              </>
            }
          />
          <SubHeading
            title={
              <>
                Lets see, How well you know <b>{requestorName}?</b>
              </>
            }
          />
        </div>
      </div>
    );
  }
};

const StartViewUserInput = ({
  color,
  setUserName,
  updateUserName,
  userName
}) => {
  return (
    <div className="mt-3 text-left">
      <SubHeading title="Enter your Full Name:" />
      <input
        type="text"
        name="fname"
        className="form-control mt-1 focus:outline-0 focus:shadow-outline h-10 border-2 border-gray-500 rounded-lg px-2 block w-full appearance-none leading-normal"
        placeholder="Full Name"
        value={userName}
        onChange={evt => updateUserName(evt)}
      />
      <div className="mt-3 text-center" data-tooltip="Enter valid name">
        <Button
          text="👉   Start  👈"
          color={color}
          onClick={e => setUserName(userName)}
        />
      </div>
    </div>
  );
};
