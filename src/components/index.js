import React from "react";

export const Heading = ({ title }) => {
  return <h3 className="text-2xl font-semibold">{title}</h3>;
};

export const SubHeading = ({ title }) => {
  return <h4 className="text-xl font-medium leading-relaxed">{title}</h4>;
};

export const Button = ({ text, onClick, color }) => {
  return (
    <button
      className="w-full p-2 font-semibold tracking-wide text-lg md:text-xl text-white text-center rounded-lg"
      style={{ background: color }}
      onClick={onClick}
    >
      {" "}
      {text}
    </button>
  );
};

export const SmallButton = ({ text, onClick, color }) => {
  return (
    <button
      className="p-2 px-3 font-semibold tracking-wide text-white text-center rounded-lg"
      style={{ background: color }}
      onClick={onClick}
    >
      {" "}
      {text}
    </button>
  );
};
