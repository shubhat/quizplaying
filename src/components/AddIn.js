import React from "react";

export class AddInTop extends React.Component {
  componentDidMount() {
    (window.adsbygoogle = window.adsbygoogle || []).push({});
  }
  render() {
    return (
      <div className="mt-2">
        <ins
          className="adsbygoogle"
          style={{ display: "inline-block", width: "300px", height: "50px" }}
          data-ad-client="ca-pub-9429667863782963"
          data-ad-slot="1340381933"
        />
      </div>
    );
  }
}

export class AddInBottom extends React.Component {
  componentDidMount() {
    (window.adsbygoogle = window.adsbygoogle || []).push({});
  }
  render() {
    return (
      <ins
        className="adsbygoogle"
        style={{ display: "block" }}
        data-ad-client="ca-pub-9429667863782963"
        data-ad-slot="6932427534"
        data-ad-format="auto"
        data-full-width-responsive="true"
      />
    );
  }
}
