import React from "react";

export const UIColorPallet = ({ setColor }) => {
  const colors = [
    "#000000",
    "#fc4d21",
    "#f27f00",
    "#172c6f",
    "#b14e00",
    "#99003b",
    "#ff4a40"
  ];

  return (
    <div className="flex pb-3">
      {colors.map((c, i) => (
        <Color key={i} color={c} setColor={e => setColor(c)} />
      ))}
    </div>
  );
};

const Color = ({ color, setColor }) => {
  return (
    <div
      className="rounded-full h-8 w-8 mr-2 color-hovered"
      onClick={setColor}
      style={{ background: color }}
    />
  );
};
