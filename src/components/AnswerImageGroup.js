import React from "react";

export const AnswerImageGroup = ({ answers, imageClicked }) => {
  const rows = [];

  for (var i = 0; i < answers.length; i = i + 2) {
    const imageGroup = [];
    if (answers[i]) {
      imageGroup.push(
        createAnswerImage(imageClicked, answers[i].url, answers[i].name, i)
      );
    }
    if (answers[i + 1]) {
      imageGroup.push(
        createAnswerImage(
          imageClicked,
          answers[i + 1].url,
          answers[i + 1].name,
          i + 1
        )
      );
    }

    rows.push(imageGroup);
  }

  return rows.map(row => (
    <div key={row[0].key} className="flex align-items justify-center mt-1">
      {row}
    </div>
  ));
};

const AnswerImage = ({ url, name, id, imageClicked }) =>
  url && (
    <div
      className={`img-${id} text-center p-2 m-3 image-hovered`}
      onClick={e => imageClicked(id)}
    >
      <img
        className="bg-center rounded-xlg shadow w-56 border"
        src={`/images/${url}`}
        alt={name}
      />
      <div className="mt-3 text-lg">{name.toUpperCase()}</div>
    </div>
  );

const createAnswerImage = (imageClicked, url, name, i) => {
  return (
    <AnswerImage
      imageClicked={imageClicked}
      url={url}
      name={name}
      key={i + 1}
      id={i + 1}
    />
  );
};
