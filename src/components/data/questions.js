export const questions = [
  {
    id: 1,
    question: "What do you put in WhatsApp status often?",
    answer: "What does {user} put in WhatsApp status often?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Memes",
        url: "b1f24a708a717d4215fe6a6c25433234.jpg"
      },
      {
        name: "Motivational Quotes",
        url: "e6790af35f8fce95b45ac78fbfb71807.jpg"
      },
      {
        name: "Love Songs",
        url: "728774758b2e6f422c83092cff29397e.jpg"
      },
      {
        name: "Others",
        url: "75434e4ed8714f541f4ffb1a508ed59a.jpg"
      }
    ]
  },
  {
    id: 2,
    question: "Do have rings in hands?",
    answer: "Does {user} have rings in hands?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Yes",
        url: "47413f6829db78fe4f4be2175808cf2e.png"
      },
      {
        name: "No",
        url: "000c7e7ce074562f22d08011b735a261.png"
      }
    ]
  },
  {
    id: 3,
    question: "What Would You Choose?",
    answer: "What would {user} choose?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Rose",
        url: "83b315ffa0af22df5310682211785de3.jpg"
      },
      {
        name: "Jasmine",
        url: "cde015a2ef4bb7edf147c87829367c12.jpg"
      },
      {
        name: "Sun Flower",
        url: "ea09423bb55056d2f2586e9fd2958052.jpg"
      },
      {
        name: "Lily",
        url: "7d591af5ca95294a207f20eb7ec4c34e.jpg"
      },
      {
        name: "Iris",
        url: "5a06a1ad564120c687fea7134b307451.jpg"
      },
      {
        name: "Marigold",
        url: "fa3fc49a6c1aa628b097871442550c07.jpg"
      },
      {
        name: "Lotus",
        url: "adf338bcf18c5fb22421d58ed055f05c.jpg"
      },
      {
        name: "Daisy",
        url: "1d5a3f8418d39083862dcaf862a2f25a.jpg"
      }
    ]
  },
  {
    id: 4,
    question: "Which Super Hero Do You Like?",
    answer: "Which Super Hero Does {user} Like?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Captain America",
        url: "2b11182d186167a123cbd6097912d9ed.jpg"
      },
      {
        name: "Spiderman",
        url: "7a9c83d0554af8eed1772d981bd251cb.jpg"
      },
      {
        name: "Superman",
        url: "cb0b15dc7d739b4c259f56bdb5e7fc66.jpg"
      },
      {
        name: "Deadpool",
        url: "90cca53c417f883e6feed2df9f81cf32.jpg"
      },
      {
        name: "Iron Man",
        url: "8d6686daea6544915817798d315684ab.png"
      },
      {
        name: "Hulk",
        url: "25098a055530289a857b310d7b32b675.png"
      },
      {
        name: "Thor",
        url: "420c2f93dadf5fd76bfcbe860af71df2.png"
      },
      {
        name: "Ant Man",
        url: "da674ef7be18c0bc121b5362c14bcaa8.jpg"
      }
    ]
  },
  {
    id: 5,
    question: "How you like to travel in your day to day life?",
    answer: "How {user} like to travel in your day to day life?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Cycle",
        url: "ff28f5da5fd01b332b5fc446460e70d1.jpg"
      },
      {
        name: "Car",
        url: "468536156dd4a213c9827a8ae6da390a.jpg"
      },
      {
        name: "Bike",
        url: "36c0199b3189dd0eeb64e18b16e07df6.jpg"
      },
      {
        name: "Bus",
        url: "6ce2c49439106e9fe90767646a7a4bea.jpg"
      },
      {
        name: "Plane",
        url: "936752b96d491f16d8ecab77107652b0.jpg"
      },
      {
        name: "Train",
        url: "1d71f1c43fb11680129e1b2d6a894901.jpg"
      }
    ]
  },
  {
    id: 6,
    question: "What exercise do you like?",
    answer: "What exercise does {user} like?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Dance",
        url: "e99de7e2835c73acfe74a1b945c3b407.jpg"
      },
      {
        name: "Yoga",
        url: "5a5b47790f9aa0912cc56e845d7213ad.jpg"
      },
      {
        name: "Gym",
        url: "9ed51e5b7f34fb682642e80c6db4f4bb.jpg"
      },
      {
        name: "Running",
        url: "a324890828e67a47cef902025ab54e26.jpg"
      },
      {
        name: "Swimming",
        url: "cbb8ef98d6004d835e9310bfedd2ba16.jpg"
      }
    ]
  },
  {
    id: 7,
    question: "Do you wear Glasses?",
    answer: "Does {user} wear Glasses?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Yes",
        url: "8a9af719f8380c3e1d123f79b11ee254.png"
      },
      {
        name: "No",
        url: "53aaa4afbf61577af6ccadc7b9ccace9.png"
      }
    ]
  },
  {
    id: 8,
    question: "What is your favorite drink?",
    answer: "What is {user} favorite drink?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Tea",
        url: "fe9bc30671e558969b5e3ee84dc83424.jpg"
      },
      {
        name: "Coffee",
        url: "673d848dd60f135d4858d10416d45d4c.jpg"
      },
      {
        name: "Juice",
        url: "5dc84dc3f12471372c3a14d9a2fe4f12.jpg"
      },
      {
        name: "Milk",
        url: "cc2dd939407ce6c96f144a45a521cf70.jpg"
      },
      {
        name: "Alcohol",
        url: "613c8209c65f6a455caf5bcc331ae8f2.jpg"
      },
      {
        name: "Water",
        url: "36710c274dcfdbe725116fd56102a30f.jpg"
      }
    ]
  },
  {
    id: 9,
    question: "Do you like beer or vodka?",
    answer: "Does {user} like beer or vodka?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Beer",
        url: "fec95f40d6097f2f8fea7b495d625b38.jpg"
      },
      {
        name: "Vodka",
        url: "ceeb8143c3f2e4dd673a38705f48fffb.jpg"
      }
    ]
  },
  {
    id: 10,
    question: "What are you good at?",
    answer: "What are {user} good at?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Swimming",
        url: "263602dc058bf029e91b23f8d6f5e892.jpg"
      },
      {
        name: "Cycling",
        url: "4c668c3ffda8b1c15aeb9269bcbfcd24.jpg"
      }
    ]
  },
  {
    id: 11,
    question: "Which type of shopping do you like?",
    answer: "Which type of shopping does {user} like?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Online",
        url: "b97e0cbdfea2d91dfc8f876cb0516505.jpg"
      },
      {
        name: "Traditional",
        url: "9f436028f8bb8581188eccfe3878b204.jpg"
      }
    ]
  },
  {
    id: 12,
    question: "Would you rather watch…?",
    answer: "Would {user} rather watch…?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Action",
        url: "638e54f18bd8f8b7c0a118a49fdb2097.jpg"
      },
      {
        name: "Romance",
        url: "509c199e455c1039a39266b3e45bf8b4.jpg"
      },
      {
        name: "Science Fiction",
        url: "c11633a33f20ed7cb04c161ed3cb1cc7.jpg"
      },
      {
        name: "Fantacy",
        url: "20bba48959deb945cbad4d3f0127b975.jpg"
      }
    ]
  },
  {
    id: 13,
    question: "What type of games do you like?",
    answer: "What type of games does {user} like?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Indoor Game",
        url: "935975447861d8e7fc97885ba4adfae6.jpg"
      },
      {
        name: "Outdoor Game",
        url: "b8f84c9f89b0fc8845686d8bb5b2529c.jpg"
      },
      {
        name: "Mobile Game",
        url: "1f312d24e5e54adcf517a44cfa370e98.jpg"
      },
      {
        name: "Video Game",
        url: "68bdda03aecebddae1c80e5f6b049ec3.jpg"
      }
    ]
  },
  {
    id: 14,
    question: "What is your Favorite dessert?",
    answer: "What is {user} Favorite dessert?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Cake",
        url: "66310142f4dfcae0a6ec48f5555832ea.jpg"
      },
      {
        name: "Pies",
        url: "a336f38b472436ba012d0e0f3313e77a.jpg"
      },
      {
        name: "Cookies",
        url: "75414df74b38be247eda6fc592ad492e.jpg"
      },
      {
        name: "Doughnuts",
        url: "7d7d5e83175dcba24c0839a5360f605b.jpg"
      },
      {
        name: "Marshmellow",
        url: "4a51957bd07cbece55534a0d9e910f4f.jpg"
      },
      {
        name: "cupcakes",
        url: "41787bfa41fc5f978a05eb783973b28b.jpg"
      }
    ]
  },
  {
    id: 15,
    question: "Which one do you like: beard or shaved?",
    answer: "Which one does {user} like: beard or shaved?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Beard",
        url: "d789d96db6f68ec6eee91d9e0347a103.jpg"
      },
      {
        name: "Shaved",
        url: "68793b55a9e2d716b5c8436124b4ab92.jpg"
      }
    ]
  },
  {
    id: 16,
    question: "What would you rather choose?",
    answer: "What would {user} rather choose?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Paragliding",
        url: "c56a33163cdfbdd443dbdd2664bcb1f8.jpg"
      },
      {
        name: "Rafting",
        url: "85a963888d9608e6dbb59b4e4959f510.jpg"
      },
      {
        name: "Skiing",
        url: "fd86a7d474798fad9b2a98ff7df14e5e.jpg"
      },
      {
        name: "Skydiving",
        url: "2f529fd5dfa74f019df6d5f27f5f221a.jpg"
      },
      {
        name: "Bungee Jumping",
        url: "f6c9e398c2919ccea264145880d32f95.jpg"
      }
    ]
  },
  {
    id: 17,
    question: "What do you use the most?",
    answer: "What does {user} use the most?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Facebook",
        url: "ae5e2a2914a1a237804585454c056943.jpg"
      },
      {
        name: "Instagram",
        url: "b5e58a2253e9b86395e255774976df22.jpg"
      },
      {
        name: "Snapchat",
        url: "7372c623fd2d5c94c152b0292254e24c.png"
      },
      {
        name: "TikTok",
        url: "ac453c437c873baa67386a7cf39fd311.jpg"
      },
      {
        name: "Whatsapp",
        url: "dfbde31420e3daee541c2e63e12fa8ff.jpg"
      },
      {
        name: "Skype",
        url: "bf7cb669e1b252fdf161a37cfa573b4f.png"
      }
    ]
  },
  {
    id: 18,
    question: "Do you love school life or college life?",
    answer: "Does {user} love school life or college life?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "School Life",
        url: "b3d3331cbe0824249498c471a2e8b74e.jpg"
      },
      {
        name: "College Life",
        url: "a2212e104bd3f37c08d7c4f82ee4fccc.jpg"
      }
    ]
  },
  {
    id: 19,
    question: "What type of weather do you prefer?",
    answer: "What type of weather does {user} prefer?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Winter",
        url: "b67a629c0048bc70ccb8bf6b2a66da94.jpg"
      },
      {
        name: "Summer",
        url: "d516bbb3b2ae8706d4d7a9608f0af25e.jpg"
      },
      {
        name: "Rainy",
        url: "5b9ff17ddf1f9cbfac93d0e2815b3ab2.jpg"
      },
      {
        name: "Autumn/Fall",
        url: "58ec75466322655200f45a6943f0f62e.jpg"
      }
    ]
  },
  {
    id: 20,
    question: "What do you prefer?",
    answer: "What Does {user} prefer?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Tea",
        url: "d88982658fa355b03a6dfadae1b066cc.jpg"
      },
      {
        name: "Coffee",
        url: "4844e6e0bfec15546284af606c23875a.jpg"
      }
    ]
  },
  {
    id: 21,
    question:
      "If your house is on fire, what is the one thing you will take with you?",
    answer:
      "If {user} house is on fire, what is the one thing {user} will take with ?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Laptop",
        url: "78502117f0c9d52fdb4ed9fcd81afac7.jpg"
      },
      {
        name: "Mobile",
        url: "85745ed3a26f940b08eecb59c44dff83.jpg"
      },
      {
        name: "Documents",
        url: "adeddfa55de2cfe1d235406a3be87349.jpg"
      },
      {
        name: "Wallet",
        url: "a8a17462597aa1f49481c3abda4fa17d.jpg"
      }
    ]
  },
  {
    id: 22,
    question: "What is your Favourite Ice-Cream?",
    answer: "Which is {user} Favourite Ice-Cream?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Chocolate",
        url: "f54cd8928a4b30410c4aa54b0de95df3.jpg"
      },
      {
        name: "Vanilla",
        url: "587ade7471cc70af0f1c41c938d2c5c7.jpg"
      },
      {
        name: "Butterscotch",
        url: "3f68c95b509bbc74c2ff6f81c62c37ed.jpg"
      },
      {
        name: "Strawberry",
        url: "27363c541feb577f6bce95b58f89356d.jpg"
      },
      {
        name: "Coffee",
        url: "dd11ccef0a0685c4a4b5b2e381aee0bc.jpg"
      },
      {
        name: "Mango",
        url: "fcf74f1df792caee5bac91f18e1f1b71.jpg"
      }
    ]
  },
  {
    id: 23,
    question: "What is more important to you?",
    answer: "What is more important to {user}?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Love",
        url: "15545cb8da6d32d3ebb2e9abfffe6b1c.jpg"
      },
      {
        name: "Money",
        url: "f530c9511961f42e8df16814fb4f98b9.jpg"
      }
    ]
  },
  {
    id: 24,
    question: "Which one do you like?",
    answer: "Which one does {user} like?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Sweet",
        url: "c56ebb7a7f8ac1eb5df2ad8bdb6f3735.jpg"
      },
      {
        name: "Spicy",
        url: "584149bcd3e5258b260e8d99b1d4cf8b.jpg"
      }
    ]
  },
  {
    id: 25,
    question: "What do you like the most?",
    answer: "What does {user} like the most?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Kiss",
        url: "0ba438c98002fbb1293d1de2444e7be5.jpg"
      },
      {
        name: "Hug",
        url: "eccc7d8fd1a46dce246f8130b69e4062.jpg"
      }
    ]
  },
  {
    id: 26,
    question: "You would like to watch..",
    answer: "What Does {user} like to watch..?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Movies At Home",
        url: "cbe10dfc5464e30c413c92fc4c158423.jpg"
      },
      {
        name: "Movies At Theatre",
        url: "9a4995226496de20117fa03bbaeab0ac.jpg"
      }
    ]
  },
  {
    id: 27,
    question: "Which one will you choose?",
    answer: "Which one {user} will choose?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Mountains",
        url: "2568625ee597c1512383f60e50f73cbe.jpg"
      },
      {
        name: "Beach",
        url: "de1607b725d106e4b5dd1e17620693d4.jpg"
      },
      {
        name: "glacier",
        url: "a9c38d901463eff6aecf1ff78639f2a3.jpg"
      },
      {
        name: "Jungle",
        url: "c7c2552cac46ba06d9e57854651339d1.jpg"
      }
    ]
  },
  {
    id: 28,
    question: "If you win a lottery what will you buy?",
    answer: "If {user} win a lottery what will He/She buy?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Private Plane",
        url: "4d9c3de08c8320421695535cf72f673d.jpg"
      },
      {
        name: "Villa",
        url: "f42e9bd78dc72415693362d906e14ebf.jpg"
      },
      {
        name: "Disney Land",
        url: "ad6c58e5a2e3a32cf077412fb4d1ccdb.jpg"
      },
      {
        name: "Diamonds",
        url: "e187c1e0064d2d165cc79a2f3dc73e57.jpg"
      }
    ]
  },
  {
    id: 29,
    question: "Who would you choose?",
    answer: "Who would {user} choose?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Obama",
        url: "09a0d92954b77e961bcd37c9b4218ca4.jpg"
      },
      {
        name: "Trump",
        url: "4c0f8810d4153f89bb357acd63786f9d.jpg"
      }
    ]
  },
  {
    id: 30,
    question: "What kind of hair do you like?",
    answer: "What Does {user} like Long hair or short hair?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Short Hair",
        url: "09c70233f0b846491fb73f870c4109db.jpg"
      },
      {
        name: "Long Hair",
        url: "4bc7324083925c7449138c65c67cabc2.jpg"
      }
    ]
  },
  {
    id: 31,
    question: "Do you like veg or non-veg?",
    answer: "What Does {user} like veg or non-veg?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Veg",
        url: "22162ae3e3d96618814531d80db5ef95.jpg"
      },
      {
        name: "Non-Veg",
        url: "22004451b154d506cd689228e21cb503.jpg"
      }
    ]
  },
  {
    id: 32,
    question: "What would you prefer?",
    answer: "What Does {user} prefer?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Tattoo",
        url: "7b2122418d74acbfbbbad0b72bb497d0.jpg"
      },
      {
        name: "No Tattoo",
        url: "275816c60b3d35087e9828c523591eee.jpg"
      }
    ]
  },
  {
    id: 33,
    question: "Do you prefer rain or snow?",
    answer: "What Does {user} prefer rain or snow?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Rain",
        url: "3b7d9030add7059386a24ef0e5c29398.jpg"
      },
      {
        name: "Snow",
        url: "0cbeea2b1f124ce7069a16b0419eb43f.jpg"
      }
    ]
  },
  {
    id: 34,
    question: "Which one you Drink Most?",
    answer: "Which one {user} Drink Most?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Cold Coffee",
        url: "aed65d20563ba69177fd9fd7811125c7.png"
      },
      {
        name: "Tea",
        url: "3a44e7fcdc720172586f5b477c7c268d.png"
      },
      {
        name: "Hot Coffee",
        url: "05eb5bbe17ca4bc7298ffbc8f7292b34.png"
      },
      {
        name: "Cold Drink",
        url: "6de159d4b19eee9cbddcaad3e29a6cde.png"
      }
    ]
  },
  {
    id: 35,
    question: "Do you like singing or dancing?",
    answer: "What Does {user} like singing or dancing?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Singing",
        url: "be06323c15a8f8e8c70db38f40396b19.jpg"
      },
      {
        name: "Dancing",
        url: "9c41a9189b13428aa66736b70746145d.jpg"
      }
    ]
  },
  {
    id: 36,
    question: "Do You have a Crush in School/College?",
    answer: "Does {user} have a Crush in School/College?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Yes",
        url: "f14395160f2fb2a30de30cc500df1f57.png"
      },
      {
        name: "No",
        url: "87950cdd02a400ad690662e29a7ef675.png"
      }
    ]
  },
  {
    id: 37,
    question: "Which life is best?",
    answer: "Which life does {user} Like?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Single",
        url: "298c90060cd05e6a48d3582ba80375a3.png"
      },
      {
        name: "Commited",
        url: "d41f6cd086eac6e1fb4620900f520af9.png"
      },
      {
        name: "One Side Love",
        url: "6b0461f981df7f3b0cbbf342c6f9838e.png"
      },
      {
        name: "Married",
        url: "6d869eef61cc0587393fc25d8fd0ec57.png"
      }
    ]
  },
  {
    id: 38,
    question: "Your Level of Laziness?",
    answer: "What is {user}'s Level of Laziness?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Very Lazy",
        url: "77bff193c5308f9f88101c011ef1945c.png"
      },
      {
        name: "Fully Active",
        url: "7e3360fe2f6d6a2e478550af10c74297.png"
      }
    ]
  },
  {
    id: 39,
    question: "If you meets a genie, what would be your wish?",
    answer: "If {user} meets a genie, what would be his/her wish?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "1 Billion dollar",
        url: "4c0fddf3c26a6db08e2f2acc2cd16ddf.jpg"
      },
      {
        name: "Luxury car",
        url: "45b5ad7503bae1d145c299ffd6e828fd.jpg"
      },
      {
        name: "King/Queen of world",
        url: "9667bbf387f945ccf5ac31147fa02421.jpg"
      },
      {
        name: "Your Love",
        url: "dbc8a995ba0522f8448fe4912574325e.jpg"
      }
    ]
  },
  {
    id: 40,
    question:
      "Do you want his/her wife/husband to be the Hottest or the Smartest?",
    answer:
      "Does {user} want his/her wife/husband to be the Hottest or the Smartest?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Hot",
        url: "4303d06376ce58402671f7d543d66f47.jpg"
      },
      {
        name: "Smart",
        url: "8b441095f98c2373ca8d7f5763c1fc04.jpg"
      }
    ]
  },
  {
    id: 41,
    question: "What is Your idea of a perfect evening?",
    answer: "What is {user} idea of a perfect evening?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Candle Light Dinner",
        url: "d226e9d221f1a0b93d1a70a71d531d7e.jpg"
      },
      {
        name: "A football game",
        url: "ec33a0486af391d23b608ebb05447447.jpg"
      },
      {
        name: "Movie with friends",
        url: "d52cd98991e2df21fe632feb3b82c937.jpg"
      },
      {
        name: "Hit a club",
        url: "f7ea4dfd3ea77585bcc921ffc86e1637.jpg"
      }
    ]
  },
  {
    id: 42,
    question: "Which fast food restaurant does you prefer the most",
    answer: "Which fast food restaurant does {user} prefer the most",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Dominos",
        url: "e3c34bfaacd2e623d35023c094e66edf.png"
      },
      {
        name: "McDonalds",
        url: "d0aefc2fa4c88c499ae05ab58f3c122e.jpg"
      },
      {
        name: "Pizza Hut",
        url: "fc938f987c586ef0196cf01472aebe0e.jpg"
      },
      {
        name: "Burger King",
        url: "a9ae511a704777cd466e3a6f2b98671a.jpg"
      }
    ]
  },
  {
    id: 43,
    question: "Where would You like to go with your soulmate?",
    answer: "Where would {user} like to go with His/her soulmate?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Dubai",
        url: "8332dfa7714282eaa643d9a10e793de7.jpg"
      },
      {
        name: "Paris",
        url: "d4800f91d595c0606394464af71c5455.jpg"
      },
      {
        name: "Bahamas",
        url: "a1adce8df3d3188456895db3638a8eba.jpg"
      },
      {
        name: "Taaj Mahal",
        url: "48990579c8c5368d6e79eb7978319521.jpg"
      }
    ]
  },
  {
    id: 44,
    question: "Which animal do you dream to pet?",
    answer: "Which animal does {user} dream to pet?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Cat",
        url: "4ca97cf6525b2777f583483d58a46166.jpg"
      },
      {
        name: "Dog",
        url: "97f545638ce407465eb7988a23e34966.jpg"
      },
      {
        name: "Rabbit",
        url: "fbe6e6b7b93fceadbd31a742c5a34fe7.jpg"
      },
      {
        name: "Hamster",
        url: "04b9114f10c4742792a9916117208a56.jpg"
      }
    ]
  },
  {
    id: 45,
    question: "In freetime, where would you go?",
    answer: "In freetime, where would {user} go?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Gym",
        url: "17efef50a34a799815029b6ce8b17549.jpg"
      },
      {
        name: "Sleeping",
        url: "0400c52b3185d484b9db5bd6ca52028d.jpg"
      },
      {
        name: "Shopping",
        url: "c77b1c7bf395964e7115faa8dce87aed.jpg"
      },
      {
        name: "Cooking",
        url: "4252994927f2cb061eedcfc70fb26eb1.jpg"
      },
      {
        name: "Reading",
        url: "476d5ec06a014230ac077874de66ee67.jpg"
      },
      {
        name: "Swimming",
        url: "0f3b6d23efa9e125e26c7e62700e0598.jpeg"
      }
    ]
  },
  {
    id: 46,
    question: "Your favourite TV series is?",
    answer: "What is {user} favourite TV series?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Game of thrones",
        url: "3e1782846166b32af754e68e2886257b.jpg"
      },
      {
        name: "Sacred Game",
        url: "ed0b0b5993814d960368f862edf90047.jpg"
      },
      {
        name: "F.R.I.E.N.D.S",
        url: "8a2927e11fe23e4310876a2816d4e3d4.jpg"
      },
      {
        name: "Mirzapur",
        url: "7e4f4335ffbfefc2639b385a4199a773.jpg"
      }
    ]
  },
  {
    id: 47,
    question: "Who is your favourite cricketer?",
    answer: "Who is {user} favourite cricketer?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "VIrat Kohli",
        url: "3282cf3d2a201fe0e56e3a45cb0af6e6.jpg"
      },
      {
        name: "Ab Devillier",
        url: "128065652914d9d5dab8117816f24b1f.jpg"
      },
      {
        name: "Sachin Tendulkar",
        url: "558fb17f9f1635b0896168b8ec1bcf00.jpg"
      },
      {
        name: "Chris Gayle",
        url: "fd0ad9623ef77965c7c3f5d4cf8b4047.jpg"
      },
      {
        name: "MS Dhoni",
        url: "4dfeeffb451774ebd2cadf4ea1a50bb4.jpeg"
      }
    ]
  },
  {
    id: 48,
    question: "Which superpower would you choose?",
    answer: "Which superpower would {user} choose?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Be invisible",
        url: "8f9a099a0abcb8a7a65e93a498466181.jpg"
      },
      {
        name: "Fly like Superman",
        url: "5f25ec93da248b03f55aed496ce7cc4e.jpg"
      },
      {
        name: "See Future",
        url: "4c3413fb8c7b53d4137866dcc6f0542a.jpg"
      },
      {
        name: "See Ghost",
        url: "9da53edca72d4bfb4a3163adfb3bdda5.jpg"
      },
      {
        name: "Read minds",
        url: "a6acbe70134f20851163f4ccc80a67f2.jpg"
      }
    ]
  },
  {
    id: 49,
    question: "Which life is best ?",
    answer: "What {user} think. Which life is best ? ",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Single",
        url: "b397bc672e49ee450289ada49d6f2eb5.jpg"
      },
      {
        name: "committed",
        url: "83ecf024ddc42aad707587ee125e60ad.jpg"
      },
      {
        name: "Married",
        url: "583609626b33c4769c0f01f234c85f8e.jpg"
      },
      {
        name: "One Side Love",
        url: "4ad96fecb99b84edae3fb05d2b73066b.jpg"
      }
    ]
  },
  {
    id: 50,
    question:
      "If you get an opportunity for an adventurous journey, who would you prefer?",
    answer:
      "If {user} get an opportunity for an adventurous journey, who would {user} prefer?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Friends",
        url: "5673e727890001b73a9934d1f31fe376.jpg"
      },
      {
        name: "Soulmate",
        url: "e8897f4254df5c092e8f3cf280288a61.jpg"
      },
      {
        name: "Family",
        url: "c3579be151fa09f970067e7d937da9a2.jpg"
      }
    ]
  },
  {
    id: 51,
    question:
      "If you get a chance to act in a movie, what character do you prefer?",
    answer:
      "If {user} get a chance to act in a movie, what character do {user} prefer?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Hero",
        url: "77a1e1f0f02f0a3261726520a759a852.jpg"
      },
      {
        name: "Villain",
        url: "b3c4e54a43c4bf6037c34891e5397270.jpg"
      },
      {
        name: "Comedian",
        url: "8b605e342ecd814830ea079df1d94323.jpg"
      }
    ]
  },
  {
    id: 52,
    question:
      "If you are a character in Aladdin Movie, what do you want to become?",
    answer:
      "If {user} are a character in Aladdin Movie, what does he/she want to become?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Aladdin",
        url: "93d3d6b99f7688aa2d14a4a21ceebfb2.jpg"
      },
      {
        name: "Genie",
        url: "568b0c6dfe6688f81e432a29119e1cd6.png"
      },
      {
        name: "Jafar",
        url: "d16307f27e55a0460d7c087b10a4223f.jpg"
      },
      {
        name: "Princess Jasmine",
        url: "8081d6229b380ec29d09ba57f108a752.jpg"
      }
    ]
  },
  {
    id: 53,
    question: "Which is your birth month?",
    answer: "Which is {user} birth month?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "January",
        url: "07df0a4d5a780e6aed261be9a27699db.jpeg"
      },
      {
        name: "February",
        url: "7abb9a1cbb189a9adecff82afda9aedc.png"
      },
      {
        name: "March",
        url: "283e4d0634791ce7f60d2a1d5a32f523.png"
      },
      {
        name: "April",
        url: "09e8401cc897b55975c9ea432fd03b8c.png"
      },
      {
        name: "May",
        url: "9030957123a11374df011152281b9010.png"
      },
      {
        name: "June",
        url: "d97c2b9ce7b1b77bd315cf69a477ef5b.png"
      },
      {
        name: "July",
        url: "170a3506e7b4f7455ec58c9804e9ad4d.png"
      },
      {
        name: "August",
        url: "99a9e0902908521a9ef92550805fa4e8.jpg"
      },
      {
        name: "September",
        url: "75cc6893849167738876f045352165a3.png"
      },
      {
        name: "October",
        url: "c2d2a69accd9b3e57c829e82d97e7f85.jpg"
      },
      {
        name: "November",
        url: "683c29ddd8169ff210a268a263abd5b4.jpg"
      },
      {
        name: "December",
        url: "56179e4da08e7b96bc3a2c6b8a64da22.jpg"
      }
    ]
  },
  {
    id: 54,
    question: "What do you prefer as a gift on your birthday?",
    answer: "What does {user} prefer as a gift on his/her birthday?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Rose Bouquet",
        url: "d1a6ec72bf1d147ff4cf358d62b0001a.jpg"
      },
      {
        name: "Giant Teddy Bear",
        url: "1f42ff91b2e11bc0f3e9de09623dbd83.jpg"
      },
      {
        name: "Chocolate",
        url: "9ad412c881423ae40166375963c39296.jpg"
      },
      {
        name: "Surprise Me",
        url: "98c6fc5c6260fd7412f6c2450cd8e834.jpg"
      }
    ]
  },
  {
    id: 55,
    question: "Who is your Favourite TikTok Star",
    answer: "Who is {user}'s Favourite TikTok Star",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Gima Ashi",
        url: "122fd2722b6c4f51b108c72d53606f12.jpg"
      },
      {
        name: "Jannat Faisu",
        url: "de7de2be1c25b178e0c1c726cc805eee.jpg"
      },
      {
        name: "Khushi Vivek",
        url: "3fe53fab258d9e73206c43ec500c1e0e.jpg"
      },
      {
        name: "Awez",
        url: "e90dc7e5431d6a4efc9f4454e94031e7.jpg"
      }
    ]
  },
  {
    id: 56,
    question: "What is your Favourite OutFit?",
    answer: "What is {user}'s Favourite OutFit?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Jeans",
        url: "082280cd745a30e58a993d24f48e9fd4.jpg"
      },
      {
        name: "Suit",
        url: "8d0e127909f48caf92dab322a6ff5b9c.jpg"
      }
    ]
  },
  {
    id: 57,
    question: "Which app do you like more?",
    answer: "What app does {user} like more?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Pubg",
        url: "8d4a641aad326d7bca3b96f0c26d07fb.jpg"
      },
      {
        name: "TikTok",
        url: "5281a635ed630f25d25cf81a5ce68e21.jpg"
      }
    ]
  },
  {
    id: 58,
    question: "What is your Favourite Singer?",
    answer: "What is {user}'s Favourite Singer?",
    hide: 0,
    featured: 0,
    images: [
      {
        name: "Guru Randhawa",
        url: "84a12c6dafcc8c7f5d0fe523db49acbb.jpg"
      },
      {
        name: "B Praak",
        url: "0f1fd8a101e5bf8a11ebf540e08c474f.jpg"
      },
      {
        name: "Parmish Verma",
        url: "c5a64e1ce846df12c133ca4aa634dc04.jpg"
      },
      {
        name: "Singga",
        url: "c6dc78e4f6a474426b6d57f230d0ebee.jpg"
      }
    ]
  },
  {
    id: 68,
    question: "What Is Your Sleeping Style?",
    answer: "What is {user}'s Sleeping Style?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Sleep on Stomach",
        url: "6909ccee92e3a6b8bde7c6bd08425bad.jpg"
      },
      {
        name: "Sleep on back",
        url: "b4c1818aa0068d4ec3f4aab12049e94f.jpg"
      },
      {
        name: "Sleep on side",
        url: "14101e540f841a41c2ba59130b79437d.jpg"
      }
    ]
  },
  {
    id: 60,
    question: "How do you like to Click Pictures?",
    answer: "How {user} like to Click Pictures?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Selfie",
        url: "2a842c2f0a273f2e777fc48562e145f2.jpg"
      },
      {
        name: "Pose",
        url: "7b882ffba88cf23d64f8a279fc848f4a.jpg"
      }
    ]
  },
  {
    id: 61,
    question: "What do you Like?",
    answer: "What {user} Like to do?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Hard Work",
        url: "c7640900b0338ae60227e92d800cb156.jpeg"
      },
      {
        name: "Smart Work",
        url: "b332950c138671d11c8a18ff5f302ed1.jpg"
      }
    ]
  },
  {
    id: 62,
    question: "Which type of wedding do you like?",
    answer: "Which type of wedding does {user} like?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Simple Marriage",
        url: "fda6f6fa78474f287267ad84cad1f106.jpg"
      },
      {
        name: "Court Marriage",
        url: "79a80aeafaaf2dd6ec2c1dda2848ca75.jpg"
      },
      {
        name: "Destination Marriage",
        url: "85b1445d8020d2653236ad818f07394f.jpg"
      },
      {
        name: "Live-in Relationship",
        url: "85b27cf1396631b19da8dd22d30adffe.jpg"
      }
    ]
  },
  {
    id: 63,
    question: "What did you wanted to be when you were a kid?",
    answer: "What {user} wanted to be when He/She was a kid?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Doctor",
        url: "2fbc1ffac9bd5c72408308e0133bded3.jpg"
      },
      {
        name: "Singer",
        url: "8fad4f03a97cbf19a2bbd6c1a2e28128.jpg"
      },
      {
        name: "Anything Else",
        url: "62a389464c73773ed2b9e3d7d9a97278.jpg"
      },
      {
        name: "Engineer",
        url: "7ee19b3e3019c465e458d74f9938eb39.jpg"
      },
      {
        name: "Astronaut",
        url: "25b9ee6913dfcde12b0c6a7ac4c3f4dc.jpg"
      },
      {
        name: "Actor",
        url: "d68edb90d9934e50d86a2c4db106bf53.jpg"
      },
      {
        name: "Lawyer",
        url: "63b5fa3c945a251f592dbdecd2f68e0d.jpg"
      },
      {
        name: "Pilot",
        url: "f576e37601aef9d540f3c0562df1228e.jpg"
      }
    ]
  },
  {
    id: 64,
    question: "How Many EX GF/BF did you have?",
    answer: "How Many EX GF/BF did {user} have?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "1",
        url: "c848d99eaeef6478b6b3541f9dd56a51.jpg"
      },
      {
        name: "2",
        url: "3f3b0b2fa1d15db9a9b796dedee7b3c5.jpg"
      },
      {
        name: "3",
        url: "b360aae2b02f89943fe66bd3799ae36e.jpg"
      },
      {
        name: "4",
        url: "fff3104697a1eb398d3b3c5180977636.jpg"
      },
      {
        name: "5+",
        url: "e3fe004aa8871968c1712751b3778fa7.jpg"
      },
      {
        name: "Zero",
        url: "ec4c7c0854eeb2ebbf3fc7a5529c7932.jpg"
      }
    ]
  },
  {
    id: 65,
    question: "In which subject you're best?",
    answer: "In which subject {user} is best?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "English",
        url: "490d4b644d176b33f6aa1096284c5e40.jpg"
      },
      {
        name: "Science",
        url: "e06ee4a2466eba7ac73cb28486863e1b.jpg"
      },
      {
        name: "History",
        url: "5e8b24e684be9b5ff04d1d5e1433146e.jpg"
      },
      {
        name: "Maths",
        url: "6c57ec6309946d7436a3bf6c0a82d124.jpg"
      },
      {
        name: "Any Other",
        url: "caa66f4fc1f4a442ee1e3a18c5bc32c7.jpg"
      }
    ]
  },
  {
    id: 66,
    question: "Which of these describe your character?",
    answer: "Which of these describe {user}'s character?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Serious",
        url: "e83f891168ecf8a936db716a08037f2e.png"
      },
      {
        name: "Funny",
        url: "c97a553c4034f6e7d1cf5a1dc9e521d6.jpg"
      },
      {
        name: "Friendly",
        url: "f8f452b84780df71685c44a24072cf34.jpg"
      },
      {
        name: "Kind",
        url: "d76206a69238e4aebb401178ec4b6a2e.jpg"
      }
    ]
  },
  {
    id: 67,
    question:
      "How you like to be greeted when you meet someone for the first time ?",
    answer:
      "How {user}'s like to be greeted when he/she meet someone for the first time ?",
    hide: 0,
    featured: 1,
    images: [
      {
        name: "Hug",
        url: "951e3125b322a750694f1dd7ef8d915c.jpg"
      },
      {
        name: "Kiss",
        url: "bdeb3798b73a8955c015ae51481fe8e0.jpg"
      },
      {
        name: "Shake hand",
        url: "d15bcbd0966705c7640c3a34359d7dff.jpg"
      },
      {
        name: "Smile",
        url: "49e4038d12016a87d62acef08b80dbe1.jpeg"
      }
    ]
  }
];
