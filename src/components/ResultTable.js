import React from "react";
import { Heading } from "../components";

export const ResultTable = ({ rows, name }) => {
  if (!name) {
    return false;
  }
  rows = rows.sort((a, b) => b.score - a.score);
  return (
    <>
      <Heading title={`Who Knows ${name} Best?`} />
      <div className="table-responsive mt-3 rounded shadow border">
        <table className="table table-bordered">
          <thead className="thead-light">
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Score</th>
            </tr>
          </thead>
          <tbody>
            {rows.map((row, key) => (
              <tr key={key}>
                <td>{row.name}</td>
                <td>{row.score}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};
