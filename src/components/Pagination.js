import React from "react";

export const Pagination = ({ size, active }) => {
  const numbers = Array.from(Array(parseInt(size)), (x, index) => index + 1);
  return numbers.map(n => {
    let classes =
      parseInt(active) === n ? "bg-green-700 text-white rounded-lg" : "";
    classes += " p-1 m-1 text-gray-700 tracking-tight font-semibold";
    return (
      <div key={n} className={classes}>
        {n}
      </div>
    );
  });
};
