const purgecss = require("@fullhuman/postcss-purgecss");
const tailwindcss = require("tailwindcss");
const cssnano = require("cssnano");
const autoprefixer = require("autoprefixer");

module.exports = {
  plugins: [
    tailwindcss("./tailwind.config.js"),
    cssnano({
      preset: "default"
    }),
    // purgecss({
    //   content: [
    //     "./src/**/*.html",
    //     "./src/**/*.vue",
    //     "./src/**/*.js",
    //     "./src/*.js"
    //   ]
    // }),
    autoprefixer
  ]
};
